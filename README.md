decopy(1) -- Automatic debian/copyright generator
=================================================

## SYNTAX

`decopy` [options] [files or dirs]

## DESCRIPTION

Decopy is a tool that automates creating and updating the debian/copyright
files.  It does this by parsing all files in a source tree, figuring out the
copyright attribution and license for each file (when possible) and then
structuring the output in the copyright format 1.0.

## USAGE

Simply running `decopy` in the source tree that needs to be parsed will process
all files in the tree, group them by license, wildcard those groups and generate
the proper output to be written to the debian/copyright file.

If the source tree already includes a debian/copyright file, it will be used as
input for files where the license could not be inferred, for the copyright of
the debian directory and for the license snippets (which need to be filled in
manually if there is no debian/copyright file).

The files or directories specified as positional arguments can be used to limit
the processing to the indicated subset instead of processing the complete source
tree.

## EXAMPLES

On top of the simple `decopy` usage, these examples show some of the most common
usecases.  Please see the following sections for detailed explanations of what
each of the parameters do.

 * `decopy --root src/hello-1.0/ --output src/hello-1.0/debian/copyright`:
   This runs the parser in the hello-1.0 source tree and generates the
   debian/copyright file in the source tree (the debian/ directory needs to
   pre-exist this call).

 * `decopy --mode partial src/3rdparty/ninja`:
   This shows the licensing and copyright information for the
   `src/3rdparty/ninja` directory. Only that directory is analyzed and the
   output includes only files contained in it.

 * `decopy --group-by copyright`:
   This processes the complete current source tree, paragraphs are split
   not only according to the licenses but also according to the copyright
   owners.

 * `decopy src`:
   This will process the src directory but, provided debian/copyright already
   exists, show the complete updated debian/copyright file.

## COMMON OPTIONS

 * `--mode <mode>`:
   Indicates the mode to use when generating the output. The **full** mode
   (default) will generate a complete debian/copyright file, even if only some
   files were processed (the rest of the contents are taken from the original
   debian/copyright file). While the **partial** mode will only print the
   copyright and licensing information for the indicated files.

 * `--output <file>`, `-o <file>`:
   Store the generated output in the indicated file. This will overwrite the
   file if it already exists.

 * `--root <path>`:
   Process the indicated path instead of the current directory.

## GROUPING CRITERIA

The generated output will always create separate paragraphs for groups of files
with different licenses, additionally, it can also split paragraphs according to
the following grouping options.

 * `--group-by <criteria>`:
   When grouping by **license** (default), all copyright owners of different
   files with the same license are listed together.  When grouping by
   **copyright**, each different group of copyright owners is listed
   separatedly (files that have the exact same list of copyright owners are
   listed together).

 * `--split-on-license`:
   When a subdirectory has its own license file (COPYING, LICENSE, etc), the
   subdirectory is given its own paragraph, even if it has the same license as
   the main directory.  This is the default.

 * `--no-split-on-license`:
   Even if a subdirectory has its own license file, the contents are listed
   together with other files in the tree that have the same license.

 * `--split-debian`:
   Even if the contents of the debian/ subdirectory have the same license as the
   main content, split it onto a separate paragraph.  This is the default.

 * `--no-split-debian`:
   If the contents of the debian/ subdirectory have the same license as the main
   content, they are listed together.

## LOG LEVEL OPTIONS

 The default logging shows WARN and ERROR messages.  It can be modified with the
 following flags:

 * `--debug`:
   Show DEBUG, INFO, WARN and ERROR messages.

 * `--verbose`, `-v`:
   Show INFO, WARN and ERROR messages.

 * `--quiet`, `-q`:
   Show only ERROR messages.

## LESS COMMON OPTIONS

 * `--copyright-file <filename>`:
   Specify a different copyright file to be used as a base for the processing.
   Defaults to `debian/copyright`

 * `--text`:
   Parse all files as text files, instead of using the different parsers
   according to the file types (e.g. using exiftool for images).

 * `--exclude <path>`, `-X <path>`:
   Exclude files from being parsed.  The path can be specified as a regular
   expression that is matched against the full relative path for the file (e.g.
   `-X '.*\.jpg$'` to exclude all files that have the `.jpg` extension. or `-X
   doc/` to exclude all files inside the `doc/` directory). As a special case,
   `debian/copyright` and `debian/changelog` are always excluded..

## COMPARISON WITH OTHER HELPERS

Decopy was inspired by previous copyright helpers like [copyright-helper by
Modestas Vainius][1], licensecheck(1) by Jonas Smedegaard, or the copyright
helper shipped with debmake(1), written by Osamu Aoki.

[1]: https://anonscm.debian.org/cgit/users/modax/copyright-helper.git/

The main difference with all of them, is that decopy aims to detects as many
licenses as possible, including many more that those in the other helpers, as
well as the fact that it's capable of processing more than just plain text
files.

Additionally, while it is possible to use it for the creation of the copyright
file, its main usecase is to keep the copyright file updated with any changes
that are done to the source tree as time passes.

## AUTHORS

Decopy was written by Maximiliano Curia.  The debian packaging and this manpage
were created by Margarita Manterola.

## COPYRIGHT

Decopy and all the associated code and documentation are released under the ISC
license.


