#!/usr/bin/env python3
# encoding: utf-8

# Unit tests for decopy
# Copyright (C) 2016 Maximiliano Curia <maxy@debian.org>
#
# License: ISC
#  Permission to use, copy, modify, and/or distribute this software for any
#  purpose with or without fee is hereby granted, provided that the above
#  copyright notice and this permission notice appear in all copies.
#  .
#  THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH
#  REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
#  AND FITNESS. IN NO EVENT SHALL ISC BE LIABLE FOR ANY SPECIAL, DIRECT,
#  INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
#  LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
#  OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
#  PERFORMANCE OF THIS SOFTWARE.

import io
import sys
import unittest

from unittest.mock import DEFAULT, patch

from decopy import cmdoptions
from decopy import output
from decopy import tree


class TestOutput(unittest.TestCase):

    def setUp(self):
        # Create one root with one dir with one file
        self.tree = tree.RootInfo(root='.')
        self.groups = {}

    def testEmpty(self):
        groups = {}
        copyright_ = None
        options = cmdoptions.process_options([])

        with patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                            new_callable=io.StringIO) as _sys:
            output.generate_output(groups, self.tree, copyright_, options)
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))
        self.assertEqual(out, 'Format: https://www.debian.org/doc/'
                         'packaging-manuals/copyright-format/1.0/\n'
                         'Source: TODO\n',
                         'Unexpected stdout: {}'.format(out))

    def testEmptyPartial(self):
        groups = {}
        copyright_ = None
        options = cmdoptions.process_options(['--mode=partial', '.'])

        with patch.multiple(sys, stdout=DEFAULT, stderr=DEFAULT,
                            new_callable=io.StringIO) as _sys:
            output.generate_output(groups, self.tree, copyright_, options)
        out = _sys['stdout'].getvalue()
        err = _sys['stderr'].getvalue()
        self.assertFalse(len(err), 'Unexpected stderr: {}'.format(err))
        self.assertEqual(out, 'Format: https://www.debian.org/doc/'
                         'packaging-manuals/copyright-format/1.0/\n'
                         'Source: TODO\n'
                         'Comment: *** only: . ***\n',
                         'Unexpected stdout: {}'.format(out))


if __name__ == '__main__':
    unittest.main()
